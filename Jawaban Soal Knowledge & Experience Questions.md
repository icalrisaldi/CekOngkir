1. Pernah, 
    - kode kotlin lebih singkat dibanding. contoh : tidak memerlukan getter setter pada model, tidak memerlukan findviewbyid ketika inisialisasi 
    - kotlin lebih bapat mencegah NullPointerExceptions dibanding java
    - kode kotlin mendekati javascript

2. - OkHttp dan Retrofit : untuk integerasi dengan API
   - SQLite dan Realm : digunakan untuk database local
   - Firebase : Database yang disediakan oleh google
   - google.vision : untuk scan QR code
   - dan lain lain

3. Masih belum sering, hanya beberapa
   - Adapter : Umum digunakan di list view. mempermudah desain dan fungsi di dalam listview
   - Builder : Saya gunakan ketika sebuah fungsi atau library membutuh kan setting terlebih dahulu, sehingga tidak memenuhi Main Activity dengan banyak setting

4. - NullPointerExeption : Melakukan error handling, ketika data tersebut null maka dilakukan tidakan pencegahnya agar aplikasi tidak force close
   - Gradle Error : Terkadang tanpa alasan grandle sering error, diatasi dengan rebuild atau clean. Pengambilan data dari API yang terlalu lama yang disebabkan mungkin koneksi yang bermasalah, memberikan error handling ketika pengambilan gagal conto "ToasT" atau mengulang proses sampai data didapat. ketika melakukan proses tersebut user dapat dialihkan dengan progress dialog sehingga proses tidak terganggu yang menyebabkan pengambilan gagal.

5. - Saya biasa menggunakan Trello untuk mencatat fitur yang diperlukan. kemudian dari fitur tersebut di buat checklist task yang perlu dikerjakan, ketika sebuah project tersebut dikerjakan oleh sebuah tim maka anggota tim akan memberi tanda task mana yang dilakukan. 
   - task yang dikerjakan dibedakan di sebuah branch tersendiri. kemudian dipindah ke branch develop untuk disatukan. ketika task sudah di test oleh tester dan lolos. task tersebut dimerge di master untuk digunakan oleh client.

6. Meningkatkan performa dengan cara melakukan InstrumentTest pada UI, Tidak mengulang ulang suatu fungsi yang sama, menggunakan Pro Guard agar aplikasi tidak mudah di decompile dan menjadikan semua API yang dipakai menjadi https sehingga lebih aman

7. Bersedia