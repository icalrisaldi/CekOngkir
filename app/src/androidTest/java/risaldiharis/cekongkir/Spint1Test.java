package risaldiharis.cekongkir;


import android.support.test.espresso.DataInteraction;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;


@LargeTest
@RunWith(AndroidJUnit4.class)
public class Spint1Test {

    @Rule
    public ActivityTestRule<Home> mActivityTestRule = new ActivityTestRule<>(Home.class);

    @Test
    public void spint1Test() {
        ViewInteraction appCompatEditText = Espresso.onView(
                Matchers.allOf(ViewMatchers.withId(R.id.edt_username),
                        childAtPosition(
                                childAtPosition(
                                        ViewMatchers.withClassName(Matchers.is("android.widget.LinearLayout")),
                                        0),
                                1),
                        ViewMatchers.isDisplayed()));
        appCompatEditText.perform(ViewActions.click());

        ViewInteraction appCompatEditText2 = Espresso.onView(
                Matchers.allOf(ViewMatchers.withId(R.id.edt_username),
                        childAtPosition(
                                childAtPosition(
                                        ViewMatchers.withClassName(Matchers.is("android.widget.LinearLayout")),
                                        0),
                                1),
                        ViewMatchers.isDisplayed()));
        appCompatEditText2.perform(ViewActions.replaceText("a"), ViewActions.closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = Espresso.onView(
                Matchers.allOf(ViewMatchers.withId(R.id.edt_password),
                        childAtPosition(
                                childAtPosition(
                                        ViewMatchers.withClassName(Matchers.is("android.widget.LinearLayout")),
                                        0),
                                2),
                        ViewMatchers.isDisplayed()));
        appCompatEditText3.perform(ViewActions.replaceText("super123"), ViewActions.closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = Espresso.onView(
                Matchers.allOf(ViewMatchers.withId(R.id.edt_username), ViewMatchers.withText("a"),
                        childAtPosition(
                                childAtPosition(
                                        ViewMatchers.withClassName(Matchers.is("android.widget.LinearLayout")),
                                        0),
                                1),
                        ViewMatchers.isDisplayed()));
        appCompatEditText4.perform(ViewActions.replaceText("admin"));

        ViewInteraction appCompatEditText5 = Espresso.onView(
                Matchers.allOf(ViewMatchers.withId(R.id.edt_username), ViewMatchers.withText("admin"),
                        childAtPosition(
                                childAtPosition(
                                        ViewMatchers.withClassName(Matchers.is("android.widget.LinearLayout")),
                                        0),
                                1),
                        ViewMatchers.isDisplayed()));
        appCompatEditText5.perform(ViewActions.closeSoftKeyboard());

        Espresso.pressBack();

        ViewInteraction appCompatButton = Espresso.onView(
                Matchers.allOf(ViewMatchers.withId(R.id.btn_login), ViewMatchers.withText("Login"),
                        childAtPosition(
                                childAtPosition(
                                        ViewMatchers.withClassName(Matchers.is("android.widget.LinearLayout")),
                                        0),
                                3),
                        ViewMatchers.isDisplayed()));
        appCompatButton.perform(ViewActions.click());

        Espresso.pressBack();

        ViewInteraction searchAutoComplete = Espresso.onView(
                Matchers.allOf(ViewMatchers.withClassName(Matchers.is("android.widget.SearchView$SearchAutoComplete")),
                        childAtPosition(
                                Matchers.allOf(ViewMatchers.withClassName(Matchers.is("android.widget.LinearLayout")),
                                        childAtPosition(
                                                ViewMatchers.withClassName(Matchers.is("android.widget.LinearLayout")),
                                                1)),
                                0),
                        ViewMatchers.isDisplayed()));
        searchAutoComplete.perform(ViewActions.replaceText("jawa"), ViewActions.closeSoftKeyboard());

        DataInteraction appCompatTextView = Espresso.onData(Matchers.anything())
                .inAdapterView(Matchers.allOf(ViewMatchers.withId(R.id.listItems),
                        childAtPosition(
                                ViewMatchers.withClassName(Matchers.is("android.widget.LinearLayout")),
                                1)))
                .atPosition(2);
        appCompatTextView.perform(ViewActions.click());

        ViewInteraction searchAutoComplete2 = Espresso.onView(
                Matchers.allOf(ViewMatchers.withClassName(Matchers.is("android.widget.SearchView$SearchAutoComplete")),
                        childAtPosition(
                                Matchers.allOf(ViewMatchers.withClassName(Matchers.is("android.widget.LinearLayout")),
                                        childAtPosition(
                                                ViewMatchers.withClassName(Matchers.is("android.widget.LinearLayout")),
                                                1)),
                                0),
                        ViewMatchers.isDisplayed()));
        searchAutoComplete2.perform(ViewActions.replaceText("mal"), ViewActions.closeSoftKeyboard());

        DataInteraction appCompatTextView2 = Espresso.onData(Matchers.anything())
                .inAdapterView(Matchers.allOf(ViewMatchers.withId(R.id.listItems),
                        childAtPosition(
                                ViewMatchers.withClassName(Matchers.is("android.widget.LinearLayout")),
                                1)))
                .atPosition(1);
        appCompatTextView2.perform(ViewActions.click());

        DataInteraction appCompatTextView3 = Espresso.onData(Matchers.anything())
                .inAdapterView(Matchers.allOf(ViewMatchers.withId(R.id.listItems),
                        childAtPosition(
                                ViewMatchers.withClassName(Matchers.is("android.widget.LinearLayout")),
                                1)))
                .atPosition(2);
        appCompatTextView3.perform(ViewActions.click());

        ViewInteraction appCompatEditText6 = Espresso.onView(
                Matchers.allOf(ViewMatchers.withId(R.id.edt_weight),
                        childAtPosition(
                                childAtPosition(
                                        ViewMatchers.withClassName(Matchers.is("android.widget.LinearLayout")),
                                        1),
                                1),
                        ViewMatchers.isDisplayed()));
        appCompatEditText6.perform(ViewActions.click());

        ViewInteraction appCompatEditText7 = Espresso.onView(
                Matchers.allOf(ViewMatchers.withId(R.id.edt_weight),
                        childAtPosition(
                                childAtPosition(
                                        ViewMatchers.withClassName(Matchers.is("android.widget.LinearLayout")),
                                        1),
                                1),
                        ViewMatchers.isDisplayed()));
        appCompatEditText7.perform(ViewActions.replaceText("1000"), ViewActions.closeSoftKeyboard());

        Espresso.pressBack();

        ViewInteraction appCompatButton2 = Espresso.onView(
                Matchers.allOf(ViewMatchers.withId(R.id.btn_cek), ViewMatchers.withText("Check"),
                        childAtPosition(
                                Matchers.allOf(ViewMatchers.withId(R.id.lin),
                                        childAtPosition(
                                                ViewMatchers.withClassName(Matchers.is("android.widget.LinearLayout")),
                                                1)),
                                2),
                        ViewMatchers.isDisplayed()));
        appCompatButton2.perform(ViewActions.click());

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
