package risaldiharis.cekongkir;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import risaldiharis.cekongkir.util.api.GetApi;
import risaldiharis.cekongkir.util.api.RetrofitBuilder;
import risaldiharis.cekongkir.util.db.DatabaseCrud;
import risaldiharis.cekongkir.util.model.DbCity;
import risaldiharis.cekongkir.util.model.DbProvince;
import risaldiharis.cekongkir.util.model.GetCityModel;
import risaldiharis.cekongkir.util.model.GetProvinceModel;

public class Welcome extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        final GetApi getApi = RetrofitBuilder.getBuild(this);


    }
}
