package risaldiharis.cekongkir.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharedData {
    public String dataLogin;
    public Context context;
    SharedPreferences reader;
    SharedPreferences.Editor editor;

    public SharedData(Context context) {
        this.context = context;
        reader = PreferenceManager.getDefaultSharedPreferences(context);
        editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
    }


    public String getData(String key) {
        dataLogin = reader.getString(key, "");
        return dataLogin;
    }

    public void removeData(String key){
        editor.remove(key).commit();
    }

    public void setDataString(String key, String data) {
        editor.putString(key, data).commit();
    }
    public void resetData(){editor.clear().commit();}
}
