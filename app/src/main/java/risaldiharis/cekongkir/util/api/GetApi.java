package risaldiharis.cekongkir.util.api;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import risaldiharis.cekongkir.util.model.GetCityModel;
import risaldiharis.cekongkir.util.model.GetProvinceModel;
import risaldiharis.cekongkir.util.model.ReturnCostModel;

public interface GetApi {
    @GET("province")
    Call<GetProvinceModel> getProvince();

    @GET("city")
    Call<GetCityModel> getCity();

    @FormUrlEncoded
    @POST("cost")
    Call<ReturnCostModel> postCost(@Field("origin") String origin, @Field("destination") String destination,
                                   @Field("weight") String weight, @Field("courier") String courier);
}
