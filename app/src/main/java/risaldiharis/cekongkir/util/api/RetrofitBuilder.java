package risaldiharis.cekongkir.util.api;

import android.content.Context;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitBuilder {
    private static OkHttpClient generateClient(Context ctx){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder().readTimeout(60, TimeUnit.SECONDS).connectTimeout(60, TimeUnit.SECONDS);
        HttpLoggingInterceptor httpLogging = new HttpLoggingInterceptor();
        httpLogging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request request = original.newBuilder()
                        .addHeader("Conten-Type", "application/x-www-form-urlencoded")
                        .addHeader("key", "0df6d5bf733214af6c6644eb8717c92c")
                        .method(original.method(), original.body())
                        .build();
                return chain.proceed(request);
            }
        });

        httpClient.addInterceptor(httpLogging);
        return httpClient.build();
    }
    public static GetApi getBuild(Context ctx){
        String url = "https://api.rajaongkir.com/starter/";
        GetApi getApi = null;
        OkHttpClient buildHttpClient = generateClient(ctx);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(buildHttpClient)
                .build();
        getApi = retrofit.create(GetApi.class);
        return getApi;
    }
}
