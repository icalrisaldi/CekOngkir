package risaldiharis.cekongkir.util;

import android.content.Context;
import android.net.ConnectivityManager;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class Utils {
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    public static String rupiahFormatter(Double nominal) {
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        return "Rp. "+kursIndonesia.format(nominal)+",-";
    }
}
