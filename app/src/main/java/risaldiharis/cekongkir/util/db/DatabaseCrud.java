package risaldiharis.cekongkir.util.db;

import io.realm.Realm;
import risaldiharis.cekongkir.util.model.DbCity;
import risaldiharis.cekongkir.util.model.DbProvince;

public class DatabaseCrud {

    public void setProvince (String province_id, String province){
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        DbProvince dbProvince = realm.createObject(DbProvince.class);
        dbProvince.setProvince_id(province_id);
        dbProvince.setProvince(province);
        realm.commitTransaction();
        realm.close();
    }

    public void setCity (String city_id, String province_id, String province, String type, String city_name, String postal_code){
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        DbCity dbCity = realm.createObject(DbCity.class);
        dbCity.setCity_id(city_id);
        dbCity.setProvince_id(province_id);
        dbCity.setProvince(province);
        dbCity.setType(type);
        dbCity.setCity_name(city_name);
        dbCity.setPostal_code(postal_code);
        realm.commitTransaction();
        realm.close();
    }

    public void deleteAll(){
        Realm mRealm = Realm.getDefaultInstance();
        mRealm.beginTransaction();
        mRealm.delete(DbProvince.class);
        mRealm.delete(DbCity.class);
        mRealm.commitTransaction();
        mRealm.close();
    }
}
