package risaldiharis.cekongkir.util.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReturnCostModel {

    @SerializedName("rajaongkir")
    @Expose
    private Rajaongkir rajaongkir;

    public Rajaongkir getRajaongkir() {
        return rajaongkir;
    }

    public void setRajaongkir(Rajaongkir rajaongkir) {
        this.rajaongkir = rajaongkir;
    }

    public class Rajaongkir {

        @SerializedName("query")
        @Expose
        private Query query;
        @SerializedName("status")
        @Expose
        private Status status;
        @SerializedName("origin_details")
        @Expose
        private OriginDetails originDetails;
        @SerializedName("destination_details")
        @Expose
        private DestinationDetails destinationDetails;
        @SerializedName("results")
        @Expose
        private List<Result> results = null;

        public Query getQuery() {
            return query;
        }

        public void setQuery(Query query) {
            this.query = query;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

        public OriginDetails getOriginDetails() {
            return originDetails;
        }

        public void setOriginDetails(OriginDetails originDetails) {
            this.originDetails = originDetails;
        }

        public DestinationDetails getDestinationDetails() {
            return destinationDetails;
        }

        public void setDestinationDetails(DestinationDetails destinationDetails) {
            this.destinationDetails = destinationDetails;
        }

        public List<Result> getResults() {
            return results;
        }

        public void setResults(List<Result> results) {
            this.results = results;
        }

        public class Query {

            @SerializedName("origin")
            @Expose
            private String origin;
            @SerializedName("destination")
            @Expose
            private String destination;
            @SerializedName("weight")
            @Expose
            private Integer weight;
            @SerializedName("courier")
            @Expose
            private String courier;

            public String getOrigin() {
                return origin;
            }

            public void setOrigin(String origin) {
                this.origin = origin;
            }

            public String getDestination() {
                return destination;
            }

            public void setDestination(String destination) {
                this.destination = destination;
            }

            public Integer getWeight() {
                return weight;
            }

            public void setWeight(Integer weight) {
                this.weight = weight;
            }

            public String getCourier() {
                return courier;
            }

            public void setCourier(String courier) {
                this.courier = courier;
            }

        }

        public class OriginDetails {

            @SerializedName("city_id")
            @Expose
            private String cityId;
            @SerializedName("province_id")
            @Expose
            private String provinceId;
            @SerializedName("province")
            @Expose
            private String province;
            @SerializedName("type")
            @Expose
            private String type;
            @SerializedName("city_name")
            @Expose
            private String cityName;
            @SerializedName("postal_code")
            @Expose
            private String postalCode;

            public String getCityId() {
                return cityId;
            }

            public void setCityId(String cityId) {
                this.cityId = cityId;
            }

            public String getProvinceId() {
                return provinceId;
            }

            public void setProvinceId(String provinceId) {
                this.provinceId = provinceId;
            }

            public String getProvince() {
                return province;
            }

            public void setProvince(String province) {
                this.province = province;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getCityName() {
                return cityName;
            }

            public void setCityName(String cityName) {
                this.cityName = cityName;
            }

            public String getPostalCode() {
                return postalCode;
            }

            public void setPostalCode(String postalCode) {
                this.postalCode = postalCode;
            }

        }

        public class DestinationDetails {

            @SerializedName("city_id")
            @Expose
            private String cityId;
            @SerializedName("province_id")
            @Expose
            private String provinceId;
            @SerializedName("province")
            @Expose
            private String province;
            @SerializedName("type")
            @Expose
            private String type;
            @SerializedName("city_name")
            @Expose
            private String cityName;
            @SerializedName("postal_code")
            @Expose
            private String postalCode;

            public String getCityId() {
                return cityId;
            }

            public void setCityId(String cityId) {
                this.cityId = cityId;
            }

            public String getProvinceId() {
                return provinceId;
            }

            public void setProvinceId(String provinceId) {
                this.provinceId = provinceId;
            }

            public String getProvince() {
                return province;
            }

            public void setProvince(String province) {
                this.province = province;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getCityName() {
                return cityName;
            }

            public void setCityName(String cityName) {
                this.cityName = cityName;
            }

            public String getPostalCode() {
                return postalCode;
            }

            public void setPostalCode(String postalCode) {
                this.postalCode = postalCode;
            }

        }

        public class Status {

            @SerializedName("code")
            @Expose
            private Integer code;
            @SerializedName("description")
            @Expose
            private String description;

            public Integer getCode() {
                return code;
            }

            public void setCode(Integer code) {
                this.code = code;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

        }

        public class Result {

            @SerializedName("code")
            @Expose
            private String code;
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("costs")
            @Expose
            private List<Cost> costs = null;

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public List<Cost> getCosts() {
                return costs;
            }

            public void setCosts(List<Cost> costs) {
                this.costs = costs;
            }

            public class Cost {

                @SerializedName("service")
                @Expose
                private String service;
                @SerializedName("description")
                @Expose
                private String description;
                @SerializedName("cost")
                @Expose
                private List<Cost_> cost = null;

                public String getService() {
                    return service;
                }

                public void setService(String service) {
                    this.service = service;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }

                public List<Cost_> getCost() {
                    return cost;
                }

                public void setCost(List<Cost_> cost) {
                    this.cost = cost;
                }

                public class Cost_ {

                    @SerializedName("value")
                    @Expose
                    private Integer value;
                    @SerializedName("etd")
                    @Expose
                    private String etd;
                    @SerializedName("note")
                    @Expose
                    private String note;

                    public Integer getValue() {
                        return value;
                    }

                    public void setValue(Integer value) {
                        this.value = value;
                    }

                    public String getEtd() {
                        return etd;
                    }

                    public void setEtd(String etd) {
                        this.etd = etd;
                    }

                    public String getNote() {
                        return note;
                    }

                    public void setNote(String note) {
                        this.note = note;
                    }

                }

            }

        }

    }

}
