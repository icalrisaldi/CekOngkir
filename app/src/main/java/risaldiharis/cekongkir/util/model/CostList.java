package risaldiharis.cekongkir.util.model;

public class CostList {
    String service, description, value, estimation, note;

    public CostList(String service, String description, String value, String estimation, String note) {
        this.service = service;
        this.description = description;
        this.value = value;
        this.estimation = estimation;
        this.note = note;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getEstimation() {
        return estimation;
    }

    public void setEstimation(String estimation) {
        this.estimation = estimation;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
