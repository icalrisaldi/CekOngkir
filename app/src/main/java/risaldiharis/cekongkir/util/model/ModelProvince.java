package risaldiharis.cekongkir.util.model;

public class ModelProvince {
    public String province_id;

    public String province;

    public ModelProvince(String province_id, String province) {
        this.province_id = province_id;
        this.province = province;
    }

    public String getProvince_id() {
        return province_id;
    }

    public void setProvince_id(String province_id) {
        this.province_id = province_id;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    @Override
    public String toString() {
        return province;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ModelProvince){
            ModelProvince c = (ModelProvince) obj;
            if(c.getProvince().equals(province) && c.getProvince_id()== province_id) return true;
        }

        return false;
    }
}
