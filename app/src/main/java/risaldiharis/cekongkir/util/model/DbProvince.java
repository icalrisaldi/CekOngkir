package risaldiharis.cekongkir.util.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class DbProvince extends RealmObject {

    public String province_id;

    public String province;


    public String getProvince_id() {
        return province_id;
    }

    public void setProvince_id(String province_id) {
        this.province_id = province_id;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }
}
