package risaldiharis.cekongkir.util.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetProvinceModel {
    @SerializedName("rajaongkir")
    @Expose
    private Rajaongkir rajaongkir;

    public Rajaongkir getRajaongkir() {
        return rajaongkir;
    }

    public void setRajaongkir(Rajaongkir rajaongkir) {
        this.rajaongkir = rajaongkir;
    }
    public class Rajaongkir {

        @SerializedName("query")
        @Expose
        private List<Object> query = null;
        @SerializedName("status")
        @Expose
        private Status status;
        @SerializedName("results")
        @Expose
        private List<Result> results = null;

        public List<Object> getQuery() {
            return query;
        }

        public void setQuery(List<Object> query) {
            this.query = query;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

        public List<Result> getResults() {
            return results;
        }

        public void setResults(List<Result> results) {
            this.results = results;
        }

        public class Result {

            @SerializedName("province_id")
            @Expose
            private String provinceId;
            @SerializedName("province")
            @Expose
            private String province;

            public String getProvinceId() {
                return provinceId;
            }

            public void setProvinceId(String provinceId) {
                this.provinceId = provinceId;
            }

            public String getProvince() {
                return province;
            }

            public void setProvince(String province) {
                this.province = province;
            }

        }

        public class Status {

            @SerializedName("code")
            @Expose
            private Integer code;
            @SerializedName("description")
            @Expose
            private String description;

            public Integer getCode() {
                return code;
            }

            public void setCode(Integer code) {
                this.code = code;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

        }
    }
}
