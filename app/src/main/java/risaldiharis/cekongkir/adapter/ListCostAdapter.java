package risaldiharis.cekongkir.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

import risaldiharis.cekongkir.R;
import risaldiharis.cekongkir.util.Utils;
import risaldiharis.cekongkir.util.model.CostList;

public class ListCostAdapter extends BaseAdapter {

    private Context mContext;
    private List<CostList> costLists;
    //Constructor


    public ListCostAdapter(Context mContext, List<CostList> costLists) {
        this.mContext = mContext;
        this.costLists = costLists;
    }

    @Override
    public int getCount() {
        return costLists.size();
    }

    @Override
    public Object getItem(int position) {
        return costLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = View.inflate(mContext, R.layout.item_list_cost, null);

        TextView txt_service = v.findViewById(R.id.txt_service);
        TextView txt_harga = v.findViewById(R.id.txt_harga);
        TextView txt_estimasi = v.findViewById(R.id.txt_estimasi);

        txt_service.setText(costLists.get(position).getService()+", "+costLists.get(position).getDescription());



        txt_harga.setText(Utils.rupiahFormatter(Double.parseDouble(costLists.get(position).getValue())));
        txt_estimasi.setText(costLists.get(position).getEstimation()+" Hari");


        /*TextView txtNama = v.findViewById(R.id.txt_nama_perusahaan);
        TextView txtSektor = v.findViewById(R.id.txt_sektor_perusahaan);

        //Set text for TextView
        *//*txtInitial.setText(costLists.get(position).getInitial());*//*
        txtNama.setText(costLists.get(position).getNama());
        txtSektor.setText(costLists.get(position).getSektor());

        ImageView txtInitial = v.findViewById(R.id.txt_initial);

        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color1 = generator.getRandomColor();

        TextDrawable drawable1 = TextDrawable.builder()
                .buildRoundRect(costLists.get(position).getInitial(), color1, 20);
        txtInitial.setImageDrawable(drawable1);

        //        //Save product id to tag
        v.setTag(costLists.get(position).getcostid());*/
        return v;
    }
}
