package risaldiharis.cekongkir;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import risaldiharis.cekongkir.adapter.ListCostAdapter;
import risaldiharis.cekongkir.util.SharedData;
import risaldiharis.cekongkir.util.Utils;
import risaldiharis.cekongkir.util.api.GetApi;
import risaldiharis.cekongkir.util.api.RetrofitBuilder;
import risaldiharis.cekongkir.util.db.DatabaseCrud;
import risaldiharis.cekongkir.util.model.CostList;
import risaldiharis.cekongkir.util.model.DbCity;
import risaldiharis.cekongkir.util.model.DbProvince;
import risaldiharis.cekongkir.util.model.GetCityModel;
import risaldiharis.cekongkir.util.model.GetProvinceModel;
import risaldiharis.cekongkir.util.model.ModelCity;
import risaldiharis.cekongkir.util.model.ModelProvince;
import risaldiharis.cekongkir.util.model.ReturnCostModel;

public class Home extends AppCompatActivity {
    Realm realm;
    Context context;
    SearchableSpinner spin_province_origin, spin_city_origin, spin_province_destination, spin_city_destination, spin_courier;
    ListView lv_cost;
    String city_origin, city_destination, courier;
    Integer statusFlag = 0;
    ProgressDialog progressDialog;
    SharedData sharedData;
    GetApi getApi;
    AutoCompleteTextView edt_city_origin, edt_city_destination;
    ProgressBar progSmall1, progSmall2;
    LinearLayout linResult;
    ImageView img_logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Realm.init(this);
        RealmConfiguration realmConfiguration =
                new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(realmConfiguration);

        context = this;
        sharedData = new SharedData(this);
        getApi = RetrofitBuilder.getBuild(context);

        realm = Realm.getDefaultInstance();
        RealmResults<DbCity> dbCity = realm.where(DbCity.class).findAll();
        RealmResults<DbProvince> dbProvinces = realm.where(DbProvince.class).findAll();

        sharedData.setDataString("username", "admin");
        sharedData.setDataString("password", "super123");
        if(sharedData.getData("status").equals("")){
            Intent intent = new Intent(Home.this, Login.class);
            startActivity(intent);
            finish();
        }
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Mengambil Data");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        if(dbCity.size() == 0 || dbProvinces.size() == 0){
            progressDialog.show();
            settingUp();
        }

        img_logout = findViewById(R.id.img_logout);
        img_logout.setOnClickListener(handleClick);

        //Local Search
        spin_province_origin = findViewById(R.id.spinner_province_origin);
        spin_province_origin.setOnItemSelectedListener(handleSelect);
        spin_province_origin.setTitle("Select Province Origin");

        spin_city_origin = findViewById(R.id.spinner_city_origin);
        spin_city_origin.setOnItemSelectedListener(handleSelect);
        spin_city_origin.setTitle("Select City Origin");
        spin_province_destination = findViewById(R.id.spinner_province_destination);
        spin_province_destination.setOnItemSelectedListener(handleSelect);
        spin_province_destination.setTitle("Select Province Destination");

        spin_city_destination = findViewById(R.id.spinner_city_destination);
        spin_city_destination.setOnItemSelectedListener(handleSelect);
        spin_city_destination.setTitle("Select City Destination");

        //Direct Search
        edt_city_origin = findViewById(R.id.edt_city_origin);
        edt_city_origin.setOnFocusChangeListener(handleFocus);
        edt_city_origin.setOnClickListener(handleClick);
        edt_city_origin.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ModelCity dbCity = (ModelCity) parent.getItemAtPosition(position);
                city_origin = dbCity.getCity_id();
            }
        });
        edt_city_destination = findViewById(R.id.edt_city_destination);
        edt_city_destination.setOnClickListener(handleClick);
        edt_city_destination.setOnFocusChangeListener(handleFocus);
        edt_city_destination.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ModelCity dbCity = (ModelCity) parent.getItemAtPosition(position);
                city_destination = dbCity.getCity_id();
            }
        });
        progSmall1 = findViewById(R.id.progSmall);
        progSmall2 = findViewById(R.id.progSmall2);

        //Result
        linResult = findViewById(R.id.result);
        spin_courier = findViewById(R.id.spinner_courier);
        spin_courier.setOnItemSelectedListener(handleSelect);
        spin_courier.setTitle("Select Courier");
        Button btn_cek = findViewById(R.id.btn_cek);
        btn_cek.setOnClickListener(handleClick);

        lv_cost = findViewById(R.id.lv_cost);

        //swapable implementation
        Switch mode = findViewById(R.id.switch1);
        mode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                LinearLayout localShipment = findViewById(R.id.localShipment);
                LinearLayout directShipment = findViewById(R.id.directShipment);
                if(isChecked){
                    localShipment.setVisibility(View.VISIBLE);
                    directShipment.setVisibility(View.GONE);
                    setSpinner();
                    linResult.setVisibility(View.GONE);
                    statusFlag = 0;
                } else {
                    localShipment.setVisibility(View.GONE);
                    directShipment.setVisibility(View.VISIBLE);
                    linResult.setVisibility(View.GONE);
                    edt_city_origin.setText("");
                    edt_city_destination.setText("");
                    statusFlag = 1;

                }
            }
        });

        setSpinner();

    }

    private AdapterView.OnItemSelectedListener handleSelect = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            switch (parent.getId()){
                case R.id.spinner_province_origin:
                    ModelProvince dbProvince_origin = (ModelProvince) parent.getSelectedItem();
                    List<ModelCity> spinDataCity = new ArrayList<>();
                    RealmResults<DbCity> getDataCity = realm.where(DbCity.class).equalTo("province_id", dbProvince_origin.getProvince_id()).findAll();
                    for(DbCity str : getDataCity){
                        if(str.getType().equals("Kabupaten")){
                            spinDataCity.add(new ModelCity(str.getCity_id(), str.getProvince_id(), str.getProvince(), str.getType(), "Kab. "+str.getCity_name(), str.getPostal_code()));
                        } else {
                            spinDataCity.add(new ModelCity(str.getCity_id(), str.getProvince_id(), str.getProvince(), str.getType(), str.getCity_name(), str.getPostal_code()));
                        }

                    }

                    spin_city_origin.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, spinDataCity));
                    break;
                case R.id.spinner_city_origin:
                    ModelCity dbCity_origin = (ModelCity) parent.getSelectedItem();
                    city_origin = dbCity_origin.getCity_id();
                    break;
                case R.id.spinner_province_destination:
                    ModelProvince dbProvince_destination = (ModelProvince) parent.getSelectedItem();
                    List<ModelCity> spinDataCity_destination = new ArrayList<>();
                    RealmResults<DbCity> getDataCity_destination = realm.where(DbCity.class).equalTo("province_id", dbProvince_destination.getProvince_id()).findAll();
                    for(DbCity str : getDataCity_destination){
                        if(str.getType().equals("Kabupaten")){
                            spinDataCity_destination.add(new ModelCity(str.getCity_id(), str.getProvince_id(), str.getProvince(), str.getType(), "Kab. "+str.getCity_name(), str.getPostal_code()));
                        } else {
                            spinDataCity_destination.add(new ModelCity(str.getCity_id(), str.getProvince_id(), str.getProvince(), str.getType(), str.getCity_name(), str.getPostal_code()));
                        }
                    }

                    spin_city_destination.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, spinDataCity_destination));
                    /*                DbProvince dbProvince_destination = (DbProvince) parent.getSelectedItem();*/
                    break;
                case R.id.spinner_city_destination:
                    ModelCity dbCity_destination = (ModelCity) parent.getSelectedItem();
                    city_destination = dbCity_destination.getCity_id();
                    break;
                case R.id.spinner_courier:
                    switch (parent.getSelectedItemPosition()){
                        case 0:
                            courier = "jne";
                            break;
                        case 1:
                            courier = "pos";
                            break;
                        case 2 :
                            courier = "tiki";
                            break;
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

/*    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }*/

    private View.OnFocusChangeListener handleFocus = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if(hasFocus){
                switch (v.getId()){
                    case R.id.edt_city_origin:
                        progSmall1.setVisibility(View.VISIBLE);
                        getDataCity(progSmall1, edt_city_origin);
                        break;
                    case R.id.edt_city_destination:
                        progSmall2.setVisibility(View.VISIBLE);
                        getDataCity(progSmall2, edt_city_destination);
                        break;
                }

            }

        }
    };
    private View.OnClickListener handleClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btn_cek:
                    InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    EditText edt_weight = findViewById(R.id.edt_weight);

                    if(edt_weight.getText().toString().equals("" +
                            "")){
                        edt_weight.setError("Berat Harus Di isi");
                    } else {
                        if(statusFlag!=0) {
                            if(edt_city_origin.getText().toString().equals("")){
                                edt_city_origin.setError("Asal Tidak Boleh Kosong");
                            } else if(edt_city_destination.getText().toString().equals("")){
                                edt_city_destination.setError("Tujuan Tidak Boleh Kosong");
                            } else {
                                if(!Utils.isNetworkConnected(context)) {
                                    Toast.makeText(context, "Tidak Ada Koneksi Internet", Toast.LENGTH_SHORT).show();
                                } else {
                                    getData(edt_weight);
                                }
                            }
                        } else {
                            if(!Utils.isNetworkConnected(context)) {
                                Toast.makeText(context, "Tidak Ada Koneksi Internet", Toast.LENGTH_SHORT).show();
                            } else {
                                getData(edt_weight);
                            }
                        }

                    }
                    break;
                case R.id.img_logout:
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);

                    builder.setTitle("Confirm");
                    builder.setMessage("Are you sure?");

                    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing but close the dialog
                            sharedData.removeData("status");
                            Intent intent = new Intent(Home.this, Login.class);
                            startActivity(intent);
                            dialog.dismiss();
                            finish();
                        }
                    });

                    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            // Do nothing
                            dialog.dismiss();
                        }
                    });

                    AlertDialog alert = builder.create();
                    alert.show();
                    break;
            }
        }
    };

    public void setSpinner(){
        List<ModelProvince> spinDataProvince = new ArrayList<>();
        RealmResults<DbProvince> getDataProvince = realm.where(DbProvince.class).findAll();
        for(DbProvince str : getDataProvince){
            spinDataProvince.add(new ModelProvince(str.getProvince_id(), str.getProvince()));
        }

        ArrayAdapter adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, spinDataProvince);
        spin_province_origin.setAdapter(adapter);
        spin_province_destination.setAdapter(adapter);
    }

    public void getData(EditText edt_weight){
        progressDialog.show();
        edt_weight.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // do something, e.g. set your TextView here via .setText()
                    InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                } else {
                    return false;
                }
            }
        });

        Call<ReturnCostModel> returnCostModelCall = getApi.postCost(city_origin, city_destination, edt_weight.getText().toString(), courier);
        returnCostModelCall.enqueue(new Callback<ReturnCostModel>() {
            @Override
            public void onResponse(Call<ReturnCostModel> call, Response<ReturnCostModel> response) {
                if(response.code()==200){

                    linResult.setVisibility(View.GONE);
                    TextView txt_city_origin = findViewById(R.id.txt_ket_city_origin);
                    TextView txt_city_destination = findViewById(R.id.txt_ket_city_destination);
                    TextView txt_courier = findViewById(R.id.txt_courier);
                    TextView txt_weight = findViewById(R.id.txt_weight);

                    txt_city_origin.setText(response.body().getRajaongkir().getOriginDetails().getType()+" "
                                            +response.body().getRajaongkir().getOriginDetails().getCityName()+", "
                                            +response.body().getRajaongkir().getOriginDetails().getProvince());

                    txt_city_destination.setText(response.body().getRajaongkir().getDestinationDetails().getType()+" "
                                                 +response.body().getRajaongkir().getDestinationDetails().getCityName()+", "
                                                 +response.body().getRajaongkir().getDestinationDetails().getProvince());

                    txt_weight.setText(response.body().getRajaongkir().getQuery().getWeight().toString()+" Gram");

                    List<CostList> costLists = new ArrayList<>();
                    ImageView img_icon_courier = findViewById(R.id.img_kog_courier);
                    for(ReturnCostModel.Rajaongkir.Result str : response.body().getRajaongkir().getResults()){
                        switch (str.getName()){
                            case "Jalur Nugraha Ekakurir (JNE)" :
                                img_icon_courier.setImageDrawable(ContextCompat.getDrawable(Home.this, R.mipmap.ic_jne));
                                break;
                            case "POS Indonesia (POS)":
                                img_icon_courier.setImageDrawable(ContextCompat.getDrawable(Home.this, R.mipmap.ic_pos));
                                break;
                            case "Citra Van Titipan Kilat (TIKI)":
                                img_icon_courier.setImageDrawable(ContextCompat.getDrawable(Home.this, R.mipmap.ic_tiki));
                                break;
                        }
                        txt_courier.setText(str.getName());
                        for(ReturnCostModel.Rajaongkir.Result.Cost stk : str.getCosts())
                            for(ReturnCostModel.Rajaongkir.Result.Cost.Cost_ stb : stk.getCost()){
                                costLists.add(new CostList(stk.getService(), stk.getDescription(), stb.getValue().toString(), stb.getEtd(), stb.getNote()));
                            }
                    }

                    ListCostAdapter adapter = new ListCostAdapter(context, costLists);
                    lv_cost.setAdapter(adapter);
                    linResult.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();

                } else if(response.code()==400){
                    Toast.makeText(context, "Weight tidak boleh lebih dari 30KG atau 30.000 gram.", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ReturnCostModel> call, Throwable t) {
                Toast.makeText(context, "Gagal Mengambil Data", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();

            }
        });
    }

    public void settingUp(){
        final LinearLayout lin = findViewById(R.id.lin);
        lin.setVisibility(View.GONE);
        final DatabaseCrud databaseCrud = new DatabaseCrud();
        databaseCrud.deleteAll();
        Call<GetProvinceModel> getProvinceModelCall = getApi.getProvince();
        getProvinceModelCall.enqueue(new Callback<GetProvinceModel>() {
            @Override
            public void onResponse(Call<GetProvinceModel> call, Response<GetProvinceModel> response) {
                for (GetProvinceModel.Rajaongkir.Result str : response.body().getRajaongkir().getResults()) {
                    databaseCrud.setProvince(str.getProvinceId(), str.getProvince());
                }
                Call<GetCityModel> getCityModelCall = getApi.getCity();
                getCityModelCall.enqueue(new Callback<GetCityModel>() {
                    @Override
                    public void onResponse(Call<GetCityModel> call, Response<GetCityModel> response) {
                        for (GetCityModel.Rajaongkir.Result str : response.body().getRajaongkir().getResults()){
                            databaseCrud.setCity(str.getCityId(), str.getProvinceId(), str.getProvince(), str.getType(), str.getCityName(), str.getPostalCode());
                        }
                    }

                    @Override
                    public void onFailure(Call<GetCityModel> call, Throwable t) {

                    }
                });

                lin.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<GetProvinceModel> call, Throwable t) {
                lin.setVisibility(View.VISIBLE);
            }
        });
    }

    public void getDataCity(final ProgressBar progressBar, final AutoCompleteTextView autoCompleteTextView){
        Call<GetCityModel> getCityModelCall = getApi.getCity();
        getCityModelCall.enqueue(new Callback<GetCityModel>() {
            @Override
            public void onResponse(Call<GetCityModel> call, Response<GetCityModel> response) {
                List<ModelCity> spinDataCity = new ArrayList<>();
                for(GetCityModel.Rajaongkir.Result str : response.body().getRajaongkir().getResults()){
                    if(str.getType().equals("Kabupaten")){
                        spinDataCity.add(new ModelCity(str.getCityId(), str.getProvinceId(), str.getProvince(), str.getType(), "Kab. "+str.getCityName(), str.getPostalCode()));
                    } else {
                        spinDataCity.add(new ModelCity(str.getCityId(), str.getProvinceId(), str.getProvince(), str.getType(), str.getCityName(), str.getPostalCode()));
                    }
                }
                autoCompleteTextView.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_dropdown_item_1line, spinDataCity));
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<GetCityModel> call, Throwable t) {

            }
        });
    }
}
