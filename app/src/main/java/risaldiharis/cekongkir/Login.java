package risaldiharis.cekongkir;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import risaldiharis.cekongkir.util.SharedData;

public class Login extends AppCompatActivity {
    Button btn_login;
    EditText edt_username, edt_password;
    SharedData sharedData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sharedData = new SharedData(this);
        btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(handleClick);
        edt_username = findViewById(R.id.edt_username);
        edt_password = findViewById(R.id.edt_password);

    }

    private View.OnClickListener handleClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_login:
                    if (edt_username.getText().toString().equals("")) {
                        edt_username.setError("Username Tidak Boleh Kosong");
                    } else if (edt_password.getText().toString().equals("")){
                        edt_password.setError("Password Tidak Boleh Kosong");
                    } else if(!sharedData.getData("username").equals(edt_username.getText().toString())){
                        edt_username.setError("Username Salah");
                    } else if(!sharedData.getData("password").equals(edt_password.getText().toString())){
                        edt_password.setError("Password Salah");
                    } else {
                        Intent intent = new Intent(Login.this, Home.class);
                        sharedData.setDataString("status", "login");
                        startActivity(intent);
                        finish();
                    }
                    break;
            }
        }
    };

    public void login(){

    }
}
